package com.wideum.glxssintegration;

import android.Manifest;
import android.hardware.usb.UsbDevice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.llvision.glass3.framework.ConnectionStatusListener;
import com.llvision.glass3.framework.DeviceListener;
import com.llvision.glass3.framework.DeviceManager;
import com.llvision.glass3.framework.LLVisionGlass3SDK;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private final String TAG = "***** llvision integration ******";
    private DeviceManager mDeviceManager;

    private static final int RC_SETTINGS_SCREEN_PERM = 123;
    private static final int RC_VIDEO_APP_PERM = 124;
    private TextView text;



    private ConnectionStatusListener mIConnectListener = new ConnectionStatusListener() {
        @Override
        public void onStatusChange(boolean isConnected) {
            Log.d(TAG, "onStatusChange isConnected = " + isConnected);
            if (isConnected) {
                //注册设备插拔监听
                try {
                    LLVisionGlass3SDK.getInstance().getDeviceManger().registerDeviceListener(llVisionDeviceListener);
                    mDeviceManager = LLVisionGlass3SDK.getInstance().getDeviceManger();
                } catch (Exception e) {
                    e.printStackTrace();
                    text.setText("Start error: please see stack trace");
                }
                if (LLVisionGlass3SDK.getInstance().isDeviceConnected()) {
                    //眼睛已经插入了
                    // TODO: 2018/4/16 Such as: open Camera/LCD/FaceDetection

                    Log.d("*****************************", "LLVisionGlass3SDK.getInstance().isDeviceConnected()");
                    text.setText("Device Connected");
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView) findViewById(R.id.statusText);


        requestPermissions();
    }


    private void requestPermissions() {
        String[] perms = {Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            bindGLXSS();
        } else {
            EasyPermissions.requestPermissions(this, "Accept permissions requirement", RC_VIDEO_APP_PERM, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
        bindGLXSS();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        text.setText("Please accept permissions");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void bindGLXSS() {
        LLVisionGlass3SDK.getInstance().init(this);
        LLVisionGlass3SDK.getInstance().setConnectionStatusListener(mIConnectListener);
    }


    private DeviceListener llVisionDeviceListener = new DeviceListener() {
        @Override
        public void onBootStart() {
            Log.d(TAG, "onBootStart");
            text.setText("onBootStart");
        }

        @Override
        public void onBootSuccess() {
            Log.d(TAG, "onBootSuccess");
            text.setText("onBootSuccess");
        }

        @Override
        public void onBootFail() {
            Log.d(TAG, "onBootFail");
            text.setText("onBootFail");
        }

        @Override
        public void onAttach(UsbDevice usbDevice) {
            Log.d(TAG, "onAttach");
            text.setText("onAttach");
        }

        @Override
        public void onDettach(UsbDevice usbDevice) {
            Log.d(TAG, "onDettach");
            text.setText("onDettach");
        }

        @Override
        public void onConnect(UsbDevice usbDevice) {
            Log.d(TAG, "onConnect");
            text.setText("onConnect");
        }

        @Override
        public void onDisconnect(UsbDevice usbDevice) {
            Log.d(TAG, "onDisconnect");
            text.setText("onDisconnect");
        }

        @Override
        public void onCancel(UsbDevice usbDevice) {
            Log.d(TAG, "onCancel");
            text.setText("onCancel");
        }

        @Override
        public void onFail(UsbDevice usbDevice, int i) {
            Log.d(TAG, "onFail");
            text.setText("onFail");
        }
    };
}
